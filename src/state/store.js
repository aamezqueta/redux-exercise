import { createStore } from 'redux';

import { rootReducer } from './reducer';

import {INITIAL_STATE} from "./constants.js";

export const store = createStore(rootReducer,INITIAL_STATE);
