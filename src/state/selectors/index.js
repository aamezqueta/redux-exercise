export const selectCurrentNumber = (state) => {
  return state.currentState.currentNumber;
};

export const selectCurrentStack = (state) => {
  return state.currentState.stack;
};
