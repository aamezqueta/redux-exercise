export const ADD_ACTION = "ADD";
export const SUB_ACTION = "SUB";
export const MULT_ACTION = "MULT";
export const DIV_ACTION = "DIV";
export const SQRT_ACTION = "SQRT";
export const SUMS_ACTION = "SUMS";
export const INTRO_ACTION = "INTRO";
export const UNDO_ACTION = "UNDO";
export const DECIMAL_DOT_ACTION = "DECIMAL_DOT";
export const ADD_TO_DISPLAYER_ACTION = "ADD_TO_DISPLAYER";

export const CLEAR_SCREEN_VALUE = "0";
export const NUMB_ZERO_BUTTON = "0";
export const DECIMAL_DOT = ".";


export const ZERO = 0;
export const ONE = 1;
export const TWO = 2;

export const ERROR_DIVIDED_BY_ZERO_MSG ="ERROR: DIVIDED BY ZERO";
export const ERROR_NEGATIVE_NUMB_IN_SQRT ="ERROR: SQRT OF A NEGATIVE NUMBER";


export const INITIAL_STATE = {
    currentState: { 
        currentNumber: "0",
        stack: []
    },
    history:[]
};