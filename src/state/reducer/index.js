import * as constants from "../constants.js";


const addIntoHistory = (newCurrentState,state)=>{
    return {
      currentState: newCurrentState,
      history: [...state.history, state.currentState]
    };
}


const addToDisplayer = (currentState, action) =>{

    if ( currentState.currentNumber === constants.CLEAR_SCREEN_VALUE &&
       action.number !== constants.NUMB_ZERO_BUTTON ){
          return {
              currentNumber: action.number,
              stack: currentState.stack
        };
    }

    return {
          currentNumber: currentState.currentNumber + action.number,
          stack: currentState.stack
        };

}


const addDecimalDot = (currentState) =>{
  if ( currentState.currentNumber.includes(constants.DECIMAL_DOT) ){
    return currentState;
  }
  return {
      currentNumber: currentState.currentNumber + constants.DECIMAL_DOT,
      stack: currentState.stack
      };

}


const addIntoStack = (currentState) =>{
    return {
      currentNumber: constants.CLEAR_SCREEN_VALUE, 
      stack:[parseFloat(currentState.currentNumber),...currentState.stack]
    };
}


const applyOperation = ( currentState, action)=>{
  const stack = currentState.stack.concat();
  try{
    let result = [action.operation(...stack)];
    stack.splice(0,action.cantArgs);
    result = result.concat(stack);

    return {
        currentNumber: constants.CLEAR_SCREEN_VALUE,
        stack: result
    };
  }
  catch( error){;
    console.log(error.message);
    return{
        error: error.message,
    };
  }
}


const operation = (currentState, action) =>{
  let newCurrentState = currentState;

  if ( newCurrentState.currentNumber !== constants.CLEAR_SCREEN_VALUE || 
     ( newCurrentState.stack.length === constants.ZERO && action.cantArgs === constants.ONE ) ||
     ( newCurrentState.stack.length === constants.ONE && action.cantArgs === constants.TWO ) ){
          
          newCurrentState = addIntoStack(newCurrentState);
  }
  
  if ( newCurrentState.stack.length < action.cantArgs   ){
      return currentState;
  }
  else{
      return applyOperation(newCurrentState,action);
  }

}

const sums = (currentState,action) =>{
  
  currentState = addIntoStack(currentState);
  return {
    currentNumber:constants.CLEAR_SCREEN_VALUE,
    stack: [action.operation(currentState.stack)]
  };
}


const undo = (state) =>{

    if ( state.history.length === constants.ZERO ){
       return state;
    }
    let history = state.history.concat();
    let newCurrentState = history.pop();

    return{
      currentState: newCurrentState,
      history: history
    }

}


export const rootReducer = (state, action) => {
  if (state === undefined) {
    return constants.INITIAL_STATE;
  }

  if ( state.currentState.currentNumber === constants.CLEAR_SCREEN_VALUE && action.number === constants.NUMB_ZERO_BUTTON ){
    return state;
  }

  if ( state.currentState.stack.length === constants.ZERO && action.cantArgs === constants.TWO  ){
    return state;
  }

  let newCurrentState = {};

  switch (action.type) {
    case constants.ADD_TO_DISPLAYER_ACTION:
      newCurrentState = addToDisplayer(state.currentState,action);
      break;
    
    case constants.INTRO_ACTION:
      newCurrentState = addIntoStack(state.currentState);
      break;
      
    case constants.ADD_ACTION:

    case constants.SUB_ACTION:

    case constants.MULT_ACTION:

    case constants.DIV_ACTION:

    case constants.SQRT_ACTION:
      newCurrentState = operation(state.currentState, action);
      break;
    
    case constants.SUMS_ACTION:
      newCurrentState = sums(state.currentState, action);
      break;

    case constants.DECIMAL_DOT_ACTION:
      newCurrentState = addDecimalDot(state.currentState);
      break;

    case constants.UNDO_ACTION:
      return undo(state);

    default:
      return state;
  }

  if ( newCurrentState.error ){
    return state;
  }

  return addIntoHistory(newCurrentState,state);
};
