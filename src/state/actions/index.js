import * as constants from "../constants.js";


const operationAdd = (x,y) =>{
  return x + y;
}


const operationSub = (x,y) =>{
  return x - y;
}


const operationMult = (x,y) =>{
  return x * y;
}


const operationDiv = (x,y) =>{
  if ( y === constants.ZERO ){
    throw new Error(constants.ERROR_DIVIDED_BY_ZERO_MSG);
  }
  return x / y;
}


const operationSqrt = (x) =>{
  if ( x < constants.ZERO ){
    throw new Error(constants.ERROR_NEGATIVE_NUMB_IN_SQRT);
  }

  return Math.sqrt(x);
}

const operationSums = (array)=>{
  return array.reduce((accumulator, currentValue) => accumulator + currentValue, 0 )
}


export const addNumberToDisplayer = (number) =>({
    type: constants.ADD_TO_DISPLAYER_ACTION,
    number: number.toString()
});

export const addIntoStack = () =>({
  type: constants.INTRO_ACTION
});



export const add = () =>({
  type: constants.ADD_ACTION,
  operation: operationAdd,
  cantArgs: 2
});

export const sub = () =>({
  type: constants.SUB_ACTION,
  operation:operationSub,
  cantArgs: 2
});

export const mult = () =>({
  type: constants.MULT_ACTION,
  operation:operationMult,
  cantArgs: 2
});

export const div = () =>({
  type: constants.DIV_ACTION,
  operation:operationDiv,
  cantArgs: 2
});

export const sums = () =>({
  type: constants.SUMS_ACTION,
  operation: operationSums
});

export const sqrt = () =>({
  type: constants.SQRT_ACTION,
  operation: operationSqrt,
  cantArgs: 1
});

export const addDecimalDot = () =>({
  type: constants.DECIMAL_DOT_ACTION
});

export const undo = () =>({
  type: constants.UNDO_ACTION
});