import { useDispatch, useSelector } from 'react-redux';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';
import styles from './Calculator.module.css';
import * as actions from 'state/actions';



const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  
  const currentNumber = useSelector(selectCurrentNumber);

  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();

  const onClickNumber = (number) => {
    const action = actions.addNumberToDisplayer(number);
    dispatch(action);
  };


  const onClick = (action) => {
    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClick(actions.addDecimalDot())}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClick(actions.add())}>+</button>
        <button onClick={() => onClick(actions.sub())}>-</button>
        <button onClick={() => onClick(actions.mult())}>x</button>
        <button onClick={() => onClick(actions.div())}>/</button>
        <button onClick={() => onClick(actions.sqrt())}>√</button>
        <button onClick={() => onClick(actions.sums())}>Σ</button>
        <button onClick={() => onClick(actions.undo())}>Undo</button>
        <button onClick={() => onClick(actions.addIntoStack())}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
